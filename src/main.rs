use clap::Parser;

mod fs;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
#[clap(about = "View and edit the metadata of files or directories", long_about = None)]
struct Args {
	#[clap(short, long, help = "A file or directory")]
	file: String,

	#[clap(short, long, help = "Display metadata of <FILE>")]
	view: bool,
}

fn main() {
	let args = Args::parse();

	if args.view {
		fs::view_metadata(&args.file);
		return;
	}

	println!("\nYou did not pass an action flag.");
	println!("You can view metadata with the -v flag. Try the following instead:");
	println!("\nmd -vf {}", args.file);
}